from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.template import loader
from django.urls import reverse
from django.db.models import F
from django.views import generic
from django.utils import timezone

from .models import Question, Choice

class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    # Rename the context object to match the name in the template
    # Alternatively, we could change the template to match the new default context names
    # In this case, it'd be "question_list" since it's a ListView for the Question model
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        # Return the last five published questions
        #return Question.objects.order_by('-pub_date')[:5]

        """
        Return the last five published questions, excluding
        those set to be published in the future
        """
        return Question.objects.filter(
            pub_date__lte=timezone.now()
        ).order_by('-pub_date')[:5]

# def index(request):
#    return HttpResponse("Hello, world. You're at my polls index.")

#def index(request):
#    latest_question_list = Question.objects.order_by('-pub_date')[:5]
#    #template = loader.get_template('polls/index.html')
#    
#    # A context is a dictionary mapping template variable names to Python objects
#    # Here we tell it that "latest_question_list" is the ordered question list from above
#    context = {
#        'latest_question_list': latest_question_list,
#    }
#    
#    # return HttpResponse(template.render(context, request))
#    
#    # Shortcut - bypass manually loading the template
#    return render(request, 'polls/index.html', context)

class DetailView(generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'

    def get_queryset(self):
        """
        Excludes any questions that aren't published yet.
        """
        return Question.objects.filter(pub_date__lte=timezone.now())

class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'

# def detail(request, question_id):
#     #try:
#     #    question = Question.objects.get(pk=question_id)
#     #except Question.DoesNotExist:
#     #    raise Http404("Question does not exist")
#     # return HttpResponse("You're looking at question %s." % question_id)
#     
#     # Shortcut - get_object_or_404
#     # get_list_or_404 is another valid shortcut
#     question = get_object_or_404(Question, pk=question_id)
#     return render(request, 'polls/detail.html', {'question': question})
# 
# def results(request, question_id):
#     #response = "You're looking at the results of question %s."
#     #return HttpResponse(response % question_id)
#     question = get_object_or_404(Question, pk=question_id)
#     return render(request, 'polls/results.html', {'question': question})

def vote(request, question_id):
    #return HttpResponse("You're voting on question %s." % question_id)
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form
        return render(request, 'polls/details.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })
    else:
        # The F function here gets the value in the database and updates it
        # This avoids race conditions which would occur when doing everything solely through Python
        selected_choice.votes = F('votes') + 1
        selected_choice.save()

        selected_choice.refresh_from_db()

        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.

        # reverse will fetch the URL from the view
        # In this case, the view named "results" has this URL: '<int:question_id>/results'
        # This helps avoid hardcoding the URL
        # The question id is passed as an argument to get the right URL
        # This redirects to the results page
        return HttpResponseRedirect(reverse('polls:results',
        args=(question.id,)))