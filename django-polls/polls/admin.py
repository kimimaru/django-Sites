from django.contrib import admin

from .models import Choice, Question

# TabularInline is a more compact way of displaying inline fields
class ChoiceInline(admin.TabularInline):#admin.StackedInline):
    model = Choice
    # "extra" is how many additional fields of this model are available by default
    # For example, if 1 field is filled out for one object, there would be 4 total
    extra = 3

# This is the default form representation of this object
# admin.site.register(Question)

class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['question_text']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    inlines = [ChoiceInline]
    
    # Display individual fields on the admin instead of just the str() for the Question object
    # "was_published_recently" can't be sorted because sorting by the output of an arbitrary method isn't supported
    list_display = ('question_text', 'pub_date', 'was_published_recently')
    
    # Add a filter box with filter options
    list_filter = ['pub_date']

    # Add a search box that will search all the "question_text" fields
    search_fields = ['question_text']

    # Restrict how many items are displayed per page (default 100)
    list_per_page = 2;

    # fields = ['pub_date', 'question_text']

admin.site.register(Question, QuestionAdmin)

# admin.site.register(Choice)
