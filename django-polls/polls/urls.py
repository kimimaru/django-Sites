from django.urls import path
from . import views

# Set the namespace here with "app_name"
app_name = 'polls'
urlpatterns = [
    # ex: /polls/
    path('', views.IndexView.as_view(), name='index'),
    #path('', views.index, name='index'),
    # ex: /polls/5/

    # Use generic views to reuse code
    # They require the primary key captured from the URL to be called "pk"
    # This replaces the "question_id" field, which is the primary key

    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    #path('<int:question_id>/', views.detail, name='detail'),
    # ex: /polls/5/results/
    # Adding "/results" puts the results in a separate page
    path('<int:pk>', views.ResultsView.as_view(), name='results'),
    #path('<int:question_id>/results', views.results, name='results'),
    # ex: /polls/5/vote/
    path('<int:question_id>/vote/', views.vote, name='vote'),
]