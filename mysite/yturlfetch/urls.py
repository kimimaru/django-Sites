from django.urls import path

from . import views

app_name = "yturlfetch"
urlpatterns = [
    path("", views.IndexView.as_view(), name="index"),
    path("<str:video_id>/", views.VideoView, name="video"),
]
