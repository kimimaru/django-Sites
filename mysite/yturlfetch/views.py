from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.views import generic
from django.urls import reverse
from django.db.models import F, Min

from .models import Video

class IndexView(generic.ListView):
    template_name = 'yturlfetch/index.html'
    
    context_object_name = 'video_list'

    def get_queryset(self):
        return Video.objects.all()

def VideoView(request, video_id):
    video = get_object_or_404(Video, pk=video_id)
    
    # https://invidio.us/api/v1/videos/video_id gets the video

    """
    <div id="player">
      <div id="player-wrap">
        ytplayer.config
          "args"
             "player_response"
               "streaming_data"
                 "formats" (array)
                   "url" (first array index)
    """

    # r = request.GET['player']['player-wrap'
    # print(r)

    return render(request, 'yturlfetch/video.html',
        { 
            'video': video,
        }
    )

