from django.contrib import admin

from .models import Video

class VideoAdmin(admin.ModelAdmin):
    fieldsets = [
        ("Video", { "fields": [ "video_id" ] }),
    ]

    list_display = [ "video_id" ]

    search_fields = [ "video_id" ]

admin.site.register(Video, VideoAdmin);

