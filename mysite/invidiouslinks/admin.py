from django.contrib import admin
from .models import Video, Comment

class CommentInline(admin.TabularInline):
    model = Comment
    extra = 1

class VideoAdmin(admin.ModelAdmin):
    inlines = [CommentInline]

    fieldsets = [
        ("Video", { "fields": [ "video_name", "video_id" ] }),
    ]

    list_display = [ "video_name" ]

    search_fields = [ "video_name" ]

admin.site.register(Video, VideoAdmin);
