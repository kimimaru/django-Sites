from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.views import generic
from django.http import HttpResponse
from django.urls import reverse
from django.db.models import F
from django import forms

from .models import Video, Comment
from .forms import CommentForm

class IndexView(generic.ListView):
    template_name = 'invidiouslinks/index.html'
    
    context_object_name = 'video_list'

    def get_queryset(self):
        return Video.objects.all()

def detail(request, video_id):
    video = get_object_or_404(Video, pk=video_id)

    # print("Request method:" + str(request.method))

    if request.method == 'POST':
        # "comment" is the name of the input field holding the text
        comment_text = request.POST.get("comment", None)

        if (comment_text is None or len(comment_text) <= 2):
            return render(request, 'invidiouslinks/detail.html', { 'video': video, 'error_message': 'Comment text is too short' })

        try:
            selected_comment = video.comment_set.create(video=video, comment_text=comment_text)
            selected_comment.save()
        except (KeyError, Video.DoesNotExist):
            return render(request, 'invidiouslinks/detail.html', { 'video': video, 'error_message': 'Invalid video ID' })

    return render(request, 'invidiouslinks/detail.html',
        { 'video': video,
        }
    )

def index(request):
    return render(request, 'invidiouslinks/index.html')

# def addcomment(request, video_id):
#     video = get_object_or_404(Video, pk=video_id)
# 
#     if request.method == 'POST':
#         # "comment" is the name of the input
#         comment_text = request.POST.get("comment", None)
# 
#         if (len(comment_text) > 0):
#             return HttpResponseRedirect(reverse("invidiouslinks:detail"), { 'video': video, 'error_message': 'Comment text is empty' })
# 
#         try:
#             selected_comment = video.comment_set.create(video=video, comment_text=comment_text)
#             selected_comment.save()
#         except (KeyError, Video.DoesNotExist):
#             return render(request, 'invidiouslinks/detail.html', { 'video': video, 'error_message': 'Invalid video ID' })
# 
#     return HttpResponseRedirect(reverse("invidiouslinks:detail", args=(video_id)))

        # # if this is a POST request we need to process the form data
    # if request.method == 'POST':
    #     # create a form instance and populate it with data from the request:
    #     form = CommentForm(request.POST)
    #     # check whether it's valid:
    #     if form.is_valid():
    #         # process the data in form.cleaned_data as required
    #         # ...
    #         # redirect to a new URL:
    #         return HttpResponseRedirect(reverse('invidiouslinks:detail', args=(video_id)))
    #         
    # # if a GET (or any other method) we'll create a blank form
    # else:
    #     form = CommentForm()
# 
    # return render(request, 'invidiouslinks/detail.html', { 'video': video, 'form': form })