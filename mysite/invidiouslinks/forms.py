from django import forms

class CommentForm(forms.Form):
    comment_text = forms.CharField(label='Enter comment here...', max_length=500)