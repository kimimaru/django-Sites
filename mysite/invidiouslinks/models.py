from django.db import models

class Video(models.Model):
    video_id = models.CharField(max_length=200, primary_key=True)
    video_name = models.CharField(max_length=100, default="VideoName")

    def __str__(self):
        return self.video_id + ": " + self.video_name

class Comment(models.Model):
    video = models.ForeignKey(Video, on_delete=models.CASCADE)
    comment_text = models.CharField(max_length=500)

    def __str__(self):
        return self.comment_text