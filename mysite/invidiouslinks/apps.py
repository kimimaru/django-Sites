from django.apps import AppConfig


class InvidiouslinksConfig(AppConfig):
    name = 'invidiouslinks'
