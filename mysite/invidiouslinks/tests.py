from django.test import TestCase
from django.urls import reverse

from .models import Video, Comment

def createVideo(video_name, video_id):
    return Video.objects.create(video_name=video_name, video_id=video_id)

def createComment(video, comment_text):
    return Comment.objects.create(video=video, comment_text=comment_text)

class VideoDetailViewTests(TestCase):
    def test_url(self):
        offshore_drift = createVideo("Wax Doctor - Offshore Drift", "41AbR5OuIE8")
        url = reverse("invidiouslinks:detail", args=(offshore_drift.video_id,))
        response = self.client.get(url)
        self.assertContains(response, str('https://invidio.us/embed/' + offshore_drift.video_id))
        #return self.assertContains(response, offshore_drift.video_id)
    
    def test_add_comment(self):
        goemon = createVideo("Goemon", "PCD0iLMph6s")
        url = reverse("invidiouslinks:detail", args=(goemon.video_id,))

        commentText = "Awesome song!"
        response = self.client.post(url, { "comment": commentText })

        self.assertContains(response, "comment")
        self.assertContains(response, commentText)

class CommentModelTest(TestCase):
    def test_comment_data(self):
        goemon = createVideo("Goemon", "PCD0iLMph6s")
        
        text = "Awesome song!"
        comment = createComment(goemon, "Awesome song!")

        self.assertEqual(comment.video, goemon)
        self.assertEqual(comment.comment_text, text)
