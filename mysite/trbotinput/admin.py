from django.contrib import admin
from .models import Exercise, ExerciseStats

class ExerciseStatsInline(admin.StackedInline):
    model = ExerciseStats
    extra = 1

class ExerciseAdmin(admin.ModelAdmin):
    inlines = [ ExerciseStatsInline ]

    fieldsets = [
        ("Exercise", { "fields": [ "exercise_name", "exercise_id", "gif_name", "correct_value" ] }),
    ]

    list_display = [ "exercise_name" ]

    search_fields = [ "exercise_id", "exercise_name" ]

admin.site.register(Exercise, ExerciseAdmin)