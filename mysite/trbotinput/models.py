from django.db import models

class Exercise(models.Model):
    exercise_id = models.PositiveIntegerField(primary_key=True, default=0)
    exercise_name = models.CharField(max_length=256)
    gif_name = models.CharField(max_length=256)
    correct_value = models.CharField(max_length=4096, default="")

    def __str__(self):
        return self.exercise_name + ": " + str(self.exercise_id)

class ExerciseStats(models.Model):
    exercise = models.OneToOneField(Exercise, on_delete=models.CASCADE, related_name="stats")
    total_answers = models.PositiveIntegerField(default=0)
    correct_answers = models.PositiveIntegerField(default=0)


ExerciseStats.short_description = "Exercise Stats"