from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.views import generic
from django.urls import reverse
from django.db.models import F, Min

from .models import Exercise

def IndexView(request):

    minID = -1

    # Find the first entry available
    try:
        first_entry = Exercise.objects.all().aggregate(min_id=Min("exercise_id"))
        minID = first_entry.get("min_id")
    except:
        minID = -1
    
    if minID < 0:
        # print("minID < 0")
        return render(request, "trbotinput/index.html")

    # print("minID is OKAY!")
    return render(request, "trbotinput/index.html", { "first_entry": minID })

def ExerciseView(request, exercise_id):
    exercise = get_object_or_404(Exercise, pk=exercise_id)

    # print("EXERCISE ID: " + str(exercise.exercise_id))

    next_entry = -1
    last_entry = True
    correct_input = False
    input_text = None
    error_message = None

    try:
        idsGreaterThan = Exercise.objects.filter(exercise_id__gt=exercise_id)
        last_entry = idsGreaterThan.count() == 0
        
        if last_entry == False:
            next_entry = idsGreaterThan.order_by("exercise_id")[0].exercise_id
    except:
        last_entry = True

    if request.method == 'POST':
        input_text = request.POST["input"]

        if len(input_text) > 0:
        
            stats = exercise.stats

            stats.total_answers = F('total_answers') + 1

            if input_text == exercise.correct_value:
                correct_input = True
                stats.correct_answers = F('correct_answers') + 1
            else:
                error_message = "Incorrect input."

            stats.save()

            # Refresh from the database to get the correct values
            stats.refresh_from_db()
        else:
            input_text = None
            error_message = "No input provided."

    # print("next_entry: " + str(next_entry) + " | last_entry: " + str(last_entry) + " | correct_input: " + str(correct_input))

    return render(request, "trbotinput/exercise.html",
        { 
            "exercise": exercise,
            "last_entry": last_entry,
            "next_entry": next_entry,
            "correct_input": correct_input,
            "input_text": input_text,
            "error_message": error_message
        }
    )