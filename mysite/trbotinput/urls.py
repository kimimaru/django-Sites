from django.urls import path

from . import views

app_name = "trbotinput"
urlpatterns = [
    path("", views.IndexView, name="index"),
    path("<int:exercise_id>/", views.ExerciseView, name="exercise"),
]
